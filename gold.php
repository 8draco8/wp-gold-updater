<?php
  include('wp-config.php');
  include('phpquery-onefile.php');
  $gold_page = 'http://lbma.oblive.co.uk/table?metal=gold&year='.date("Y").'&type=daily';
  phpQuery::newDocumentFileHTML($gold_page);
  $goldElement = pq('body > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4)');
  $gold_price_html = $goldElement->html();
  $gold_string_substr = htmlentities($gold_price_html);

  $silver_page = 'http://lbma.oblive.co.uk/table?metal=silver&year='.date("Y").'&type=daily';
  phpQuery::newDocumentFileHTML($silver_page);
  $silverElement = pq('body > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(3)');
  $silver_price_html = $silverElement->html();
  $silver_string_substr = htmlentities($silver_price_html);

  $jewellery_price_substr = $gold_string_substr; //+ 62.2069536;  this part adds 2 pounds to jewellery price
  $silver_price_value = $silver_string_substr * 0.80; //0.75743 | multiplier is used here to have the same price as http://hattongardenmetals.com
  $coin_page = file_get_contents("http://www.hattongardenmetals.com/gold-coins.aspx");
  $coin_doc = new DOMDocument();
  $coin_doc->loadHTML($coin_page);
  $coin_full_sov = $coin_doc->getElementsByTagName('span');
  foreach($coin_full_sov as $coin ) {
    if ($coin->getAttribute('id') === 'ctl00_Content_CoinPrices_lblFullSovereign') {
      $coin_full_sov_string = $coin->textContent;
    }
  }
  $coin_half_sov = $coin_doc->getElementsByTagName('span');
  foreach($coin_half_sov as $coin ) {
    if ($coin->getAttribute('id') === 'ctl00_Content_CoinPrices_lblHavlfSovereign') {
      $coin_half_sov_string = $coin->textContent;
    }
  }
  $coin_full_kruger = $coin_doc->getElementsByTagName('span');
  foreach($coin_full_kruger as $coin ) {
    if ($coin->getAttribute('id') === 'ctl00_Content_CoinPrices_lblFullKruger') {
      $coin_full_kruger_string = $coin->textContent;
    }
  }
  $coin_half_kruger = $coin_doc->getElementsByTagName('span');
  foreach($coin_half_kruger as $coin ) {
    if ($coin->getAttribute('id') === 'ctl00_Content_CoinPrices_lblHalfKruger') {
      $coin_half_kruger_string = $coin->textContent;
    }
  }
  $coin_qtr_kruger = $coin_doc->getElementsByTagName('span');
  foreach($coin_qtr_kruger as $coin ) {
    if ($coin->getAttribute('id') === 'ctl00_Content_CoinPrices_lblQtrKruger') {
      $coin_qtr_kruger_string = $coin->textContent;
    }
  }
  $coin_tenth_kruger = $coin_doc->getElementsByTagName('span');
  foreach($coin_tenth_kruger as $coin ) {
    if ($coin->getAttribute('id') === 'ctl00_Content_CoinPrices_lblTenthKruger') {
      $coin_tenth_kruger_string = $coin->textContent;
    }
  }   
  if ( !empty($gold_string_substr) AND !empty($silver_price_value) AND !empty($jewellery_price_substr) AND !empty($coin_full_sov_string) AND !empty($coin_half_sov_string) AND !empty($coin_full_kruger_string) AND !empty($coin_half_kruger_string) AND !empty($coin_qtr_kruger_string) AND !empty($coin_tenth_kruger_string) AND !is_null($gold_string_substr) AND !is_null($silver_price_value) AND !is_null($jewellery_price_substr) AND !is_null($coin_full_sov_string) AND !is_null($coin_half_sov_string) AND !is_null($coin_full_kruger_string) AND !is_null($coin_half_kruger_string) AND !is_null($coin_qtr_kruger_string) AND !is_null($coin_tenth_kruger_string) AND is_numeric($gold_string_substr) AND is_numeric($silver_price_value) AND is_numeric($jewellery_price_substr) AND is_numeric($coin_full_sov_string) AND is_numeric($coin_half_sov_string) AND is_numeric($coin_full_kruger_string) AND is_numeric($coin_half_kruger_string) AND is_numeric($coin_qtr_kruger_string) AND is_numeric($coin_tenth_kruger_string)) {
    $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die("Error " . mysqli_error($link));
    $query = "SELECT * FROM wp_options WHERE option_id=1931" or die("Error in the query.." . mysqli_error($link));
    $result = $link->query($query);
    if($result->num_rows == 1) {
      $result->data_seek(0);
      $row = $result->fetch_assoc();
      $arr = unserialize($row['option_value']);
      $arr['gold_price'] = (string)$gold_string_substr;
      $arr['silver_price'] = (string)$silver_price_value;
      $arr['jewellery_price'] = (string)$jewellery_price_substr;
      $arr['full_sov_price'] = (string)$coin_full_sov_string;
      $arr['half_sov_price'] = (string)$coin_half_sov_string;
      $arr['full_kruger_price'] = (string)$coin_full_kruger_string;
      $arr['half_kruger_price'] = (string)$coin_half_kruger_string;
      $arr['qtr_kruger_price'] = (string)$coin_qtr_kruger_string;
      $arr['tenth_kruger_price'] = (string)$coin_tenth_kruger_string;
      $serialized = mysql_real_escape_string(serialize($arr));
      $update_query = "UPDATE wp_options SET option_value='" . $serialized . "' WHERE option_id=1931" or die("Error in the update_query.." . mysqli_error($link));
      $result2 = $link->query($update_query);
    }
    $to      = 'andrew@mediaorb.co.uk';
    $subject = 'Gold Update';
    $message = '<html><body><br>Gold price:<b>'.$gold_string_substr.'</b>';
    $message.= '<br>Silver price:<b>'.$silver_price_value.'</b>';
    $message.= '<br>Jewellery price:<b>'.$jewellery_price_substr.'</b>';
    $message.= '<br>Full Sov price:<b>'.$coin_full_sov_string.'</b>';
    $message.= '<br>Half Sov price:<b>'.$coin_half_sov_string.'</b>';
    $message.= '<br>Full Kruger price:<b>'.$coin_full_kruger_string.'</b>';
    $message.= '<br>Half Kruger Price:<b>'.$coin_half_kruger_string.'</b>';
    $message.= '<br>Qtr Kruger price:<b>'.$coin_qtr_kruger_string.'</b>';
    $message.= '<br>Tenth Kruger price:<b>'.$coin_tenth_kruger_string.'</b></body></html>';
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers.= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers.= 'From: noreply@my-goldbuyer.co.uk'. "\r\n";
    $headers.= 'Reply-To: noreply@my-goldbuyer.co.uk'."\r\n";
    $headers.= 'X-Mailer: PHP/'. phpversion();
    wp_mail($to, $subject, $message, $headers);
  } else {  
    $to      = 'andrew@mediaorb.co.uk';
    $subject = 'ERROR! Gold Updater ERROR!';
    $message = 'Ummm, yeah, its not working http://my-goldbuyer.co.uk/wp-admin/';    
    $message.= '<html><body><br>Gold price:<b>'.$gold_string_substr.'</b>';
    $message.= '<br>Silver price:<b>'.$silver_price_value.'</b>';
    $message.= '<br>Jewellery price:<b>'.$jewellery_price_substr.'</b>';
    $message.= '<br>Full Sov price:<b>'.$coin_full_sov_string.'</b>';
    $message.= '<br>Half Sov price:<b>'.$coin_half_sov_string.'</b>';
    $message.= '<br>Full Kruger price:<b>'.$coin_full_kruger_string.'</b>';
    $message.= '<br>Half Kruger Price:<b>'.$coin_half_kruger_string.'</b>';
    $message.= '<br>Qtr Kruger price:<b>'.$coin_qtr_kruger_string.'</b>';
    $message.= '<br>Tenth Kruger price:<b>'.$coin_tenth_kruger_string.'</b></body></html>';
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers.= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers.= 'From: noreply@my-goldbuyer.co.uk'. "\r\n";
    $headers.= 'Reply-To: noreply@my-goldbuyer.co.uk'."\r\n";
    $headers.= 'X-Mailer: PHP/'. phpversion();
    wp_mail($to, $subject, $message, $headers);
  }
?>